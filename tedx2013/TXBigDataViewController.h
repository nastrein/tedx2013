//
//  TXBigDataViewController.h
//  tedx2013
//
//  Created by Nolan Astrein on 3/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TXBigDataViewControllerDelegate <NSObject>

@required
- (void)didDismissBigDataView;

@end

@interface TXBigDataViewController : UIViewController

@property (weak, nonatomic) id<TXBigDataViewControllerDelegate> delegate;

@end
