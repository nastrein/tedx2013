//
//  TXSpeakersCell.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXSpeakersCell.h"

@implementation TXSpeakersCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.speakerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0., 0., 158., 158.)];
        self.speakerImageView.frame = CGRectOffset(self.speakerImageView.frame, 1.0f, 1.0f);
        [self.contentView addSubview:self.speakerImageView];
        
        self.nameLabel = [[UILabel alloc] init];
        [self.nameLabel setFrame:CGRectMake(0., frame.size.height - 32., frame.size.width - 2., 30.)];
        [self.nameLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
        [self.nameLabel setTextColor:[UIColor whiteColor]];//colorWithRed:90./255. green:91./255. blue:93./255. alpha:1.]];
        [self.nameLabel setBackgroundColor:[UIColor colorWithRed:0/255. green:0./255. blue:0./255. alpha:0.6]];
        [self.nameLabel setTextAlignment:NSTextAlignmentLeft];
        [self.speakerImageView addSubview:self.nameLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
