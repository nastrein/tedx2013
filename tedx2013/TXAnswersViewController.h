//
//  TXAnswersViewController.h
//  tedx2013
//
//  Created by Nolan Astrein on 3/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TXAnswersViewControllerDelegate <NSObject>

@required
- (void)didDismissAnswerView;

@end

@interface TXAnswersViewController : UIViewController

@property (weak, nonatomic) id<TXAnswersViewControllerDelegate> delegate;

@end
