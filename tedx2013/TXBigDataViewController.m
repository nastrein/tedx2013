//
//  TXBigDataViewController.m
//  tedx2013
//
//  Created by Nolan Astrein on 3/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXBigDataViewController.h"

static const CGFloat kNavbarWidth = 320.;
static const CGFloat kNavbarHeight = 44.;
static const CGFloat kNavbarItemSize = 44.;

@interface TXBigDataViewController ()

@property (strong, nonatomic) UIView *navbarView;
@property (strong, nonatomic) UIButton *navbarButton;
@property (strong, nonatomic) UIButton *flipButton;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIImageView *dataImageView;

@end

@implementation TXBigDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    /* set up navbar */
    self.navbarView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., kNavbarWidth, kNavbarHeight)];
    [self.navbarView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar"]]];
    [self.view addSubview:self.navbarView];
    
    self.navbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navbarButton setImage:[UIImage imageNamed:@"navbarButton"] forState:UIControlStateNormal];
    [self.navbarButton setFrame:CGRectMake(0., 0., kNavbarItemSize, kNavbarItemSize)];
    [self.navbarButton addTarget:self action:@selector(dismissBigDataView) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.navbarButton];
    
    self.flipButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - kNavbarItemSize, 0., kNavbarItemSize, kNavbarItemSize)];
    [self.flipButton setBackgroundImage:[UIImage imageNamed:@"fliparrows"] forState:UIControlStateNormal];
    [self.flipButton addTarget:self action:@selector(flipPage) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.flipButton];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0., kNavbarHeight, 320., 320.)];
    [self.webView.scrollView setScrollEnabled:NO];
    [self.webView.scrollView setBounces:NO];
    [self.view addSubview:self.webView];
    
    self.dataImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 320. + kNavbarHeight, 320., 160. - kNavbarHeight)];
    [self.dataImageView setImage:[UIImage imageNamed:@"data"]];
    [self.view addSubview:self.dataImageView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://dl.tedxuofm.com/vis/"]]];
}

- (void)dismissBigDataView
{
    [self.delegate didDismissBigDataView];
}

- (void)flipPage
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
