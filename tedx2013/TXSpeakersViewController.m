//
//  TXSpeakersViewController.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXSpeakersViewController.h"
#import "TXSpeakersCollectionViewFlowLayout.h"
#import "TXSpeakersCell.h"
#import "TXSpeakerDetailsViewController.h"
#import "TXSpeaker.h"
#import "TXAnswersViewController.h"
#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

static const CGFloat kNavbarWidth = 320.;
static const CGFloat kNavbarHeight = 44.;
static const CGFloat kNavbarItemSize = 44.;
static const CGFloat kButtonWidth = 80.;
static const CGFloat kButtonHeight = 50.;

@interface TXSpeakersViewController ()
<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate , UICollectionViewDataSource, NSURLConnectionDelegate, TXAnswersViewControllerDelegate, UITableViewDelegate, UITableViewDataSource> {
@private
    NSMutableData *responseData;
    NSArray *jsonResponse;
    
    int currentYear;
    NSMutableArray *speakersCollection;
    NSMutableArray *speakersCollection2010;
    NSMutableArray *speakersCollection2011;
    NSMutableArray *speakersCollection2012;
    
    NSMutableArray *speakersCollection2013;
    NSArray *speakers2013Names;
    NSArray *speakers2013Times;
    
    CATransition *transition;
}

@property (strong, nonatomic) UIView *navbarView;
@property (strong, nonatomic) UIButton *button2010;
@property (strong, nonatomic) UIButton *button2011;
@property (strong, nonatomic) UIButton *button2012;
@property (strong, nonatomic) UIButton *button2013;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UIButton *navbarButton;
@property (strong, nonatomic) UIButton *dataButton;

@property (strong, nonatomic) TXSpeakerDetailsViewController *detailsViewController;
@property (strong, nonatomic) TXAnswersViewController *answersViewController;

@property (strong, nonatomic) NSMutableDictionary *speakerImagesDictionary;

@property (strong, nonatomic) UITableView *tableView;

@end

@implementation TXSpeakersViewController

- (id)init
{
    self = [super init];
    if (self) {
        [self loadSpeakersDictionary];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    jsonResponse = [[NSArray alloc] init];
    responseData = [NSMutableData data];
    
    //configure layout
    TXSpeakersCollectionViewFlowLayout *layout = [[TXSpeakersCollectionViewFlowLayout alloc] init];
    
    //initialize collection view
    CGRect frame = CGRectMake(0., kButtonHeight + kNavbarHeight, self.view.frame.size.width, self.view.frame.size.height - (kButtonHeight + kNavbarHeight));
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:frame collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    [self.collectionView registerClass:[TXSpeakersCell class] forCellWithReuseIdentifier:@"TXSpeakersCell"];
    [self.view addSubview:self.collectionView];
    
    /* set up navbar */
    self.navbarView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., kNavbarWidth, kNavbarHeight)];
    [self.navbarView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar"]]];
    [self.view addSubview:self.navbarView];
    
    self.navbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navbarButton setImage:[UIImage imageNamed:@"navbarButton"] forState:UIControlStateNormal];
    [self.navbarButton setFrame:CGRectMake(0., 0., kNavbarItemSize, kNavbarItemSize)];
    [self.navbarButton addTarget:self action:@selector(dismissSpeakersView) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.navbarButton];
    
    self.dataButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.dataButton setImage:[UIImage imageNamed:@"dataButton"] forState:UIControlStateNormal];
    [self.dataButton setFrame:CGRectMake(276., 0., kNavbarItemSize, kNavbarItemSize)];
    [self.dataButton addTarget:self action:@selector(showDataView) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.dataButton];

    /* set up toggle year buttons */
    self.button2010 = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button2010 setImage:[UIImage imageNamed:@"2010_grey"] forState:UIControlStateNormal];
    [self.button2010 setImage:[UIImage imageNamed:@"2010_dark"] forState:UIControlStateSelected];
    [self.button2010 setTag:2010];
    [self.button2010 setFrame:CGRectMake(0., kNavbarHeight, kButtonWidth, kButtonHeight)];
    [self.button2010 addTarget:self action:@selector(loadSpeakersWithYear:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button2010];
    
    self.button2011 = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button2011 setImage:[UIImage imageNamed:@"2011_grey"] forState:UIControlStateNormal];
    [self.button2011 setImage:[UIImage imageNamed:@"2011_dark"] forState:UIControlStateSelected];
    [self.button2011 setTag:2011];
    [self.button2011 setFrame:CGRectMake(kButtonWidth, kNavbarHeight, kButtonWidth, kButtonHeight)];
    [self.button2011 addTarget:self action:@selector(loadSpeakersWithYear:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button2011];
    
    self.button2012 = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button2012 setImage:[UIImage imageNamed:@"2012_grey"] forState:UIControlStateNormal];
    [self.button2012 setImage:[UIImage imageNamed:@"2012_dark"] forState:UIControlStateSelected];
    [self.button2012 setTag:2012];
    [self.button2012 setFrame:CGRectMake(kButtonWidth * 2, kNavbarHeight, kButtonWidth, kButtonHeight)];
    [self.button2012 addTarget:self action:@selector(loadSpeakersWithYear:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button2012];
    
    self.button2013 = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button2013 setImage:[UIImage imageNamed:@"2013_grey"] forState:UIControlStateNormal];
    [self.button2013 setImage:[UIImage imageNamed:@"2013_dark"] forState:UIControlStateSelected];
    [self.button2013 setTag:2013];
    [self.button2013 setFrame:CGRectMake(kButtonWidth * 3, kNavbarHeight, kButtonWidth, kButtonHeight)];
    [self.button2013 addTarget:self action:@selector(loadSpeakersWithYear:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button2013];
    
    /* set up tableview */
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0., kNavbarHeight + kButtonHeight, 320., self.view.frame.size.height - kNavbarHeight - kButtonHeight)];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];
    
    speakersCollection = [NSMutableArray array];
    speakersCollection2010 = [NSMutableArray array];
    speakersCollection2011 = [NSMutableArray array];
    speakersCollection2012 = [NSMutableArray array];
    
    currentYear = 2013;
    [self loadSpeakersWithYear:self.button2013];
    
    /* set up details view controller */
    self.detailsViewController = [[TXSpeakerDetailsViewController alloc] init];
    [self.detailsViewController.view setFrame:self.view.frame];
    
    /* set up answers view controller */
    self.answersViewController = [[TXAnswersViewController alloc] init];
    [self.answersViewController.view setFrame:self.view.frame];
    [self.answersViewController setDelegate:self];
    
    transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
}

- (void)loadSpeakersDictionary
{
    self.speakerImagesDictionary = [[NSMutableDictionary alloc] init];
    
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"ann_marie-sastry"] forKey:@201113];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Chris_VanAllsburg"] forKey:@201114];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Darshan_Karwat"] forKey:@201105];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"donia_jarrar"] forKey:@201110];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Erik_Hall"] forKey:@201115];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"homes_gordon"] forKey:@201100];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"violinquartet2011"] forKey:@201116];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"jameson_toole"] forKey:@201101];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Jared_Genser"] forKey:@201117];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"kathleen_sienko"] forKey:@201109];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Madeline_Huberth"] forKey:@201102];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Marvin_Ammori"] forKey:@201104];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"mawuli_gyakobo"] forKey:@201106];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Monica_Poncedeleon"] forKey:@201108];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"parag_patil"] forKey:@201107];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Paul_Courant"] forKey:@201118];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Robert_Bartlett"] forKey:@201103];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Scott_Page"] forKey:@201112];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Thomas_Zurbuchen"] forKey:@201111];
    
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Elias_Schewel"] forKey:@201200];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"detroit-treads"] forKey:@201216];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"filmic_productions"] forKey:@201214];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"jacob_bourjaily"] forKey:@201208];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"James_Reeves"] forKey:@201209];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Jeff_McCabe"] forKey:@201204];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"jim_toy"] forKey:@201210];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"JimBagian"] forKey:@201201];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Joe_Trumpey"] forKey:@201215];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"julie_bateman_and_ethan"] forKey:@201211];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Libby_Ashton"] forKey:@201203];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Ralph_Williams"] forKey:@201205];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Rebecca_Coulborn"] forKey:@201217];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"sharon_glotzer"] forKey:@201206];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Susan_Dorr_Gould"] forKey:@201207];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Tatiana_Hofmans"] forKey:@201202];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"trevor_weltman"] forKey:@201212];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"Campus_Piano_Quartet"] forKey:@201218];
    [self.speakerImagesDictionary setObject:[UIImage imageNamed:@"ryankrasnoo"] forKey:@201213];
    
    speakersCollection2013 = [[NSMutableArray alloc] init];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Robert Quinn"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Kathryn Clark"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Oliver Uberti"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Pedro Lowenstein"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"dan morse"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Sterling Speirn"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"John Bacon"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"SharonPomerantz"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"lunch"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Mike Barwis"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Gina Athena Ulysse"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Julie Steiner"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"michael williams"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Merry Walker"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"snack"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"James Roberts"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Evelyn Alsultany"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Mary Heinen"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"zafarrazzacki"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"David Chesney"]];    
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Melissa Gross"]];
    [speakersCollection2013 addObject:[UIImage imageNamed:@"Chris Armstrong"]];
    
    speakers2013Names = @[@"Robert Quinn", @"Kathryn Clark", @"Oliver Uberti", @"Pedro Lowenstein & Maria Castro", @"Dan Morse", @"Sterling Speirn", @"John Bacon", @"Sharon Pomerantz", @"Lunch", @"Mike Barwis", @"Gina Athena Ulysse", @"Julie Steiner", @"Michael Williams", @"Merry Michelle Walker", @"Snack", @"James Roberts", @"Evelyn Alsultany", @"Mary Heinen", @"Zafar Razzacki", @"David Chesney", @"Melissa Gross", @"Chris Armstrong"];
    
    speakers2013Times = @[@"9:55", @"10:10", @"10:22", @"10:50", @"11:05", @"11:17", @"11:28", @"11:40", @"11:55", @"1:05", @"1:22", @"1:37", @"2:09", @"2:19", @"2:30", @"3:15", @"3:30", @"3:44", @"4:10", @"4:24", @"4:34", @"4:48"];
}

- (void)loadSpeakersWithYear:(id)sender {
        
    currentYear = [(UIButton *)sender tag];
    
    if(currentYear == 2010) {
        [self.button2010 setSelected:YES];
        [self.button2011 setSelected:NO];
        [self.button2012 setSelected:NO];
        [self.button2013 setSelected:NO];
        [self.tableView setHidden:YES];
        if([speakersCollection2010 count] > 0) {
            [speakersCollection setArray:speakersCollection2010];
            [self.collectionView reloadData];
            return;
        }
    }
    else if(currentYear == 2011) {
        [self.button2010 setSelected:NO];
        [self.button2011 setSelected:YES];
        [self.button2012 setSelected:NO];
        [self.button2013 setSelected:NO];
        [self.tableView setHidden:YES];
        if([speakersCollection2011 count] > 0) {
            [speakersCollection setArray:speakersCollection2011];
            [self.collectionView reloadData];
            return;
        }
    }
    else if(currentYear == 2012) {
        [self.button2010 setSelected:NO];
        [self.button2011 setSelected:NO];
        [self.button2012 setSelected:YES];
        [self.button2013 setSelected:NO];
        [self.tableView setHidden:YES];
        if([speakersCollection2012 count] > 0) {
            [speakersCollection setArray:speakersCollection2012];
            [self.collectionView reloadData];
            return;
        }
    }
    else if(currentYear == 2013) {
        [self.button2010 setSelected:NO];
        [self.button2011 setSelected:NO];
        [self.button2012 setSelected:NO];
        [self.button2013 setSelected:YES];
        [self.tableView setHidden:NO];
        [self.tableView reloadData];
        return;
    }
    if(currentYear != 2013) {
        [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
        
        NSString *url = [NSString stringWithFormat:@"http://api.tedxuofm.com/talks/year/%d", currentYear];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connection start];
    }
}

- (void)dismissSpeakersView
{
    [self.delegate didDismissView];
}

- (void)didDismissAnswerView
{
    [self dismissSpeakersView];
}

- (void)showDataView
{
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:self.answersViewController animated:NO completion:nil];
}

#pragma mark - UICollectionView DataSource Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [speakersCollection count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TXSpeakersCell * cell = (TXSpeakersCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"TXSpeakersCell" forIndexPath:indexPath];
    if(currentYear == 2010) {
        [cell.speakerImageView setImage:[(TXSpeaker *)[speakersCollection objectAtIndex:indexPath.item] thumbnail]];
    }
    else {
        int speakerID = [(TXSpeaker *)[speakersCollection objectAtIndex:indexPath.item] speakerID];
        UIImage *cellImage = (UIImage *)[self.speakerImagesDictionary objectForKey:[NSNumber numberWithInt:speakerID]];
        [cell.speakerImageView setImage:cellImage];
        
        [(TXSpeaker *)[speakersCollection objectAtIndex:indexPath.item] setThumbnail:cellImage];
    }
    [cell.nameLabel setText:[NSString stringWithFormat:@"  %@",[(TXSpeaker *)[speakersCollection objectAtIndex:indexPath.item] name]]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.detailsViewController configureForSpeaker:(TXSpeaker *)[speakersCollection objectAtIndex:indexPath.item]];
    [self presentViewController:self.detailsViewController animated:YES completion:nil];
}

#pragma mark - NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [SVProgressHUD dismiss];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aww Snap"
														message:[NSString stringWithFormat:@"You are not connected to the Internet."]
													   delegate:self cancelButtonTitle:@"Ok"
											  otherButtonTitles:nil];
	[alertView show];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [SVProgressHUD dismiss];
    
    NSError *error = nil;
    if (responseData == nil) {
        return;
    }
    
    jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData
                                                   options:NSJSONReadingAllowFragments
                                                     error:&error];
    
    for(NSDictionary *dict in jsonResponse) {
        TXSpeaker *speaker = [[TXSpeaker alloc] initFromDataObject:dict];

        if (currentYear == 2010) {
            [speakersCollection2010 addObject:speaker];
            [speakersCollection setArray:speakersCollection2010];
        }
        else if (currentYear == 2011) {
            [speakersCollection2011 addObject:speaker];
            [speakersCollection setArray:speakersCollection2011];
        }
        else if (currentYear == 2012) {
            [speakersCollection2012 addObject:speaker];
            [speakersCollection setArray:speakersCollection2012];
        }
    }
    
    NSComparator comp = ^NSComparisonResult(id a, id b) {
        NSString *first = [(TXSpeaker *)a name];
        NSString *second = [(TXSpeaker *)b name];
        return [first compare:second];
    };
    
    [speakersCollection setArray:[speakersCollection sortedArrayUsingComparator:comp]];
    [self.collectionView setContentOffset:CGPointZero animated:YES];
    [self.collectionView reloadData];
}

#pragma mark - UITableView delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return speakersCollection2013.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160.;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Custom";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[speakersCollection2013 objectAtIndex:indexPath.row]];
    [imageView setFrame:CGRectMake(0., 0., 320., 160.)];
    [cell addSubview:imageView];
    
    UILabel *name = [[UILabel alloc] init];
    [name setFrame:CGRectMake(0., 120., 320., 40.)];
    [name setBackgroundColor:[UIColor colorWithRed:0. green:0. blue:0. alpha:.6]];
    [name setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.]];
    [name setTextColor:[UIColor whiteColor]];
    [name setText:[NSString stringWithFormat:@"  %@",[speakers2013Names objectAtIndex:indexPath.row]]];
    [cell addSubview:name];
    
    UILabel *time = [[UILabel alloc] init];
    [time setFrame:CGRectMake(320.- 70, 0., 70., 40.)];
    [time setBackgroundColor:[UIColor colorWithRed:0. green:0. blue:0. alpha:.6]];
    [time setFont:[UIFont fontWithName:@"Helvetica" size:16.]];
    [time setTextColor:[UIColor whiteColor]];
    [time setTextAlignment:NSTextAlignmentCenter];
    [time setText:[speakers2013Times objectAtIndex:indexPath.row]];
    [cell addSubview:time];
    
	return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
