//
//  TXSpeakersCollectionViewFlowLayout.h
//  tedx2013
//
//  Created by Nolan Astrein on 2/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TXSpeakersCollectionViewFlowLayout : UICollectionViewFlowLayout {
    UICollectionViewScrollDirection _scrollDirection;
}

- (UICollectionViewScrollDirection)scrollDirection;

@property (nonatomic) UICollectionViewScrollDirection scrollDirection;

@end
