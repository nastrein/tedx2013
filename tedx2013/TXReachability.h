//
//  TXReachability.h
//  tedx2013
//
//  Created by Nolan Astrein on 3/12/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TXReachability : NSObject

+ (BOOL)hasInternetConnection;

@end
