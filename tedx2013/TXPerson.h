//
//  TXPerson.h
//  tedx2013
//
//  Created by Nolan Astrein on 3/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXSerializable.h"

@interface TXPerson : NSObject <TXSerializable>

@property (assign, nonatomic) int personID;
@property (assign, nonatomic, getter = canShare) bool share;
@property (strong, nonatomic) NSString *affiliation;
@property (strong, nonatomic) NSString *department;
@property (strong, nonatomic) NSString *story1;
@property (strong, nonatomic) NSString *story2;
@property (strong, nonatomic) NSString *story3;

@end
