//
//  UITextView+DisableCopyPaste.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/26/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "UITextView+DisableCopyPaste.h"

@implementation UITextView (DisableCopyPaste)

- (BOOL)canBecomeFirstResponder
{
    return NO;
}

@end
