//
//  TXAnswersViewController.m
//  tedx2013
//
//  Created by Nolan Astrein on 3/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXAnswersViewController.h"
#import "TXBigDataViewController.h"
#import "TXPerson.h"
#import "SVProgressHUD.h"
#import "UITextView+DisableCopyPaste.h"

static const CGFloat kNavbarWidth = 320.;
static const CGFloat kNavbarHeight = 44.;
static const CGFloat kNavbarItemSize = 44.;

@interface TXAnswersViewController ()
<NSURLConnectionDelegate, TXBigDataViewControllerDelegate>
{
@private
    NSMutableData *responseData;
    NSArray *jsonResponse;
    NSURLConnection *connection;
    TXPerson *person;
    
    UITextView *questionView;
    UILabel *question;
    UILabel *userInfo;
    UIImageView *instructionBox;
    UIButton *nextPerson;
    UIButton *flip;
    NSArray *questionTitles;
}

@property (strong, nonatomic) UISwipeGestureRecognizer *pageUp;
@property (strong, nonatomic) UISwipeGestureRecognizer *pageDown;
@property (assign, nonatomic) int curID;

@property (strong, nonatomic) UIView *navbarView;
@property (strong, nonatomic) UIButton *navbarButton;

@property (strong, nonatomic) TXBigDataViewController *bigDataViewController;

@end

@implementation TXAnswersViewController

- (id)init
{
    self = [super init];
    if(self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    questionTitles = [[NSArray alloc] initWithObjects:@"  My passion for ___ is unmatched",
                      @"  What did you last question?",
                      @"  What is your idea worth spreading?", nil];
    
    /* set up navbar */
    self.navbarView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., kNavbarWidth, kNavbarHeight)];
    [self.navbarView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar"]]];
    [self.view addSubview:self.navbarView];
    
    self.navbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navbarButton setImage:[UIImage imageNamed:@"navbarButton"] forState:UIControlStateNormal];
    [self.navbarButton setFrame:CGRectMake(0., 0., kNavbarItemSize, kNavbarItemSize)];
    [self.navbarButton addTarget:self action:@selector(dismissAnswersView) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.navbarButton];
    
    flip = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - kNavbarItemSize, 0., kNavbarItemSize, kNavbarItemSize)];
    [flip setBackgroundImage:[UIImage imageNamed:@"fliparrows"] forState:UIControlStateNormal];
    [flip addTarget:self action:@selector(flipPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:flip];
    
    userInfo = [[UILabel alloc] initWithFrame:CGRectMake(0., 44., self.view.frame.size.width, 40.)];
    [userInfo setBackgroundColor:[UIColor clearColor]];
    [userInfo setFont:[UIFont fontWithName:@"GillSans" size:14.]];
    [userInfo setAdjustsFontSizeToFitWidth:YES];
    [userInfo setAdjustsLetterSpacingToFitWidth:YES];
    [userInfo setTextColor:[UIColor blackColor]];
    [self.view addSubview:userInfo];
    
    question = [[UILabel alloc] initWithFrame:CGRectMake(0., 84., self.view.frame.size.width, 50.)];
    [question setBackgroundColor: [UIColor lightGrayColor]];
    [question setFont: [UIFont fontWithName:@"GillSans" size:20.]];
    [question setTextColor: [UIColor blackColor]];
    [question setText:[questionTitles objectAtIndex:0]];
    [self.view addSubview:question];
    
    questionView = [[UITextView alloc] initWithFrame:CGRectMake(0., 134., self.view.frame.size.width, self.view.frame.size.height- 50)];
    [questionView setBackgroundColor:[UIColor clearColor]];
    [questionView setEditable: NO];
    [questionView setFont:[UIFont fontWithName:@"GillSans" size:20.]];
    [questionView setTextColor:[UIColor darkGrayColor]];
    [questionView setTextAlignment: NSTextAlignmentCenter];
    [self.view addSubview:questionView];
    
    instructionBox = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 120 , self.view.frame.size.width, 70)];
    [instructionBox setImage:[UIImage imageNamed:@"instructionBox"]];
    [self.view addSubview:instructionBox];
    
    nextPerson = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 50 , self.view.frame.size.width, 50)];
    [nextPerson setBackgroundColor:[UIColor darkGrayColor]];
    [nextPerson.titleLabel setFont:[UIFont fontWithName:@"GillSans" size:20]];
    [nextPerson setTitle:@"New TEDxer" forState:UIControlStateNormal];
    [nextPerson addTarget:self action:@selector(loadStoryGroup) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextPerson];
    
    self.pageUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleUpSwipe)];
    self.pageUp.direction = UISwipeGestureRecognizerDirectionUp;
    self.pageDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleDownSwipe)];
    self.pageDown.direction = UISwipeGestureRecognizerDirectionDown;
    
    self.bigDataViewController = [[TXBigDataViewController alloc] init];
    [self.bigDataViewController.view setFrame:self.view.frame];
    [self.bigDataViewController setDelegate:self];
    
    connection = [[NSURLConnection alloc] init];
    jsonResponse = [[NSArray alloc] init];
    responseData = [NSMutableData data];
    self.curID = 10001;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self loadStoryGroup];
    [instructionBox setAlpha:1.0];
    [self fadeInstructionBox];
    
    UIWindow *delegateWindow = [[[UIApplication sharedApplication] delegate] window];
    [delegateWindow addGestureRecognizer:self.pageUp];
    [delegateWindow addGestureRecognizer:self.pageDown];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [instructionBox setHidden:NO];
    
    UIWindow *delegateWindow = [[[UIApplication sharedApplication] delegate] window];
    [delegateWindow removeGestureRecognizer:self.pageUp];
    [delegateWindow removeGestureRecognizer:self.pageDown];
}

- (void)dismissAnswersView
{
    [self.delegate didDismissAnswerView];
}

- (void)didDismissBigDataView
{
    [self dismissAnswersView];
}

- (void)fadeInstructionBox
{
    [UIView animateWithDuration:0.3
                          delay:3.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         instructionBox.alpha = 0;
                     }
                     completion:nil];
}

- (void)flipPage
{
    self.bigDataViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:self.bigDataViewController animated:YES completion:nil];
}

-(void)handleUpSwipe
{
    if ([question.text isEqual:[questionTitles objectAtIndex:0]]){
        question.text = [questionTitles objectAtIndex:1];
        [questionView setText:[person story2]];
        
    }
    else if ([question.text isEqual:[questionTitles objectAtIndex:1]]){
        question.text = [questionTitles objectAtIndex:2];
        [questionView setText:[person story3]];
    }
}

-(void)handleDownSwipe
{
    if ([question.text isEqual:[questionTitles objectAtIndex:1]]){
        question.text = [questionTitles objectAtIndex:0];
        [questionView setText:[person story1]];
        
    }
    else if ([question.text isEqual:[questionTitles objectAtIndex:2]]){
        question.text = [questionTitles objectAtIndex:1];
        [questionView setText:[person story2]];
    }
}

- (void) loadStoryGroup
{    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    self.curID = arc4random() % (11468 - 10001 +1) + 10001;
    NSString *url = [NSString stringWithFormat:@"http://api.tedxuofm.com/people/id/%d", self.curID];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection start]; 
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aww Snap"
														message:[NSString stringWithFormat:@"You are not connected to the internet"]
													   delegate:self cancelButtonTitle:@"Ok"
											  otherButtonTitles:nil];
	[alertView show];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [SVProgressHUD dismiss];
    
    NSError *error = nil;
    if (responseData == nil) {
        return;
    }
    
    jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData
                                                   options:NSJSONReadingAllowFragments
                                                     error:&error];
    
    if ([jsonResponse count]){
        
        person = [[TXPerson alloc] initFromDataObject:jsonResponse[0]];
        
        if ([person canShare]){

            if([[person department] isEqualToString:@""])
                [userInfo setText: [NSString stringWithFormat: @"  %@", [[person affiliation] capitalizedString]]];
            else
                [userInfo setText: [NSString stringWithFormat: @"  %@, %@", [[person affiliation] capitalizedString], [[person department] capitalizedString]]];
            
            [question setText:[questionTitles objectAtIndex:0]];
            [questionView setText:[person story1]];
            [questionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        }
        else
            [self loadStoryGroup];
        
    }
    else
        [self loadStoryGroup];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
