//
//  TXHomeViewController.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/19/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXHomeViewController.h"
#import "TXSpeakersViewController.h"
#import "TXTumblrViewController.h"
#import <QuartzCore/QuartzCore.h>

static const CGFloat kNavbarWidth = 320.;
static const CGFloat kNavbarHeight = 44.;
static const CGFloat kNavbarItemSize = 44.;

@interface TXHomeViewController () <TXSpeakersViewControllerDelegate> {
    CATransition *transition;
}

@property (strong, nonatomic) UIButton *conferenceButton;
@property (strong, nonatomic) UIButton *newsButton;
@property (strong, nonatomic) TXSpeakersViewController *speakersViewController;
@property (strong, nonatomic) TXTumblrViewController *tumblrViewController;

@property (strong, nonatomic) UIView *navbarView;
@property (strong, nonatomic) UIButton *navbarButton;

@end

@implementation TXHomeViewController

- (id)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    
    self.conferenceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (screenRect.size.height == 568) {
        [self.conferenceButton setBackgroundImage:[UIImage imageNamed:@"conferencebutton_568h"] forState:UIControlStateNormal];
    }
    else {
        [self.conferenceButton setBackgroundImage:[UIImage imageNamed:@"conferencebutton"] forState:UIControlStateNormal];
    }
    [self.conferenceButton setFrame:CGRectMake(0., -1., screenRect.size.width, screenRect.size.height/2 + 1.)];
    [self.conferenceButton addTarget:self action:@selector(openToConference) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.conferenceButton];
    
    self.newsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (screenRect.size.height == 568) {
        [self.newsButton setBackgroundImage:[UIImage imageNamed:@"newsbutton_568h"] forState:UIControlStateNormal];
    }
    else {
        [self.newsButton setBackgroundImage:[UIImage imageNamed:@"newsbutton"] forState:UIControlStateNormal];
    }
    [self.newsButton setFrame:CGRectMake(0., screenRect.size.height/2, screenRect.size.width, screenRect.size.height/2)];
    [self.newsButton addTarget:self action:@selector(openToNews) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.newsButton];
    
    self.speakersViewController = [[TXSpeakersViewController alloc] init];
    [self.speakersViewController setDelegate:self];
    
    self.tumblrViewController = [[TXTumblrViewController alloc] init];
    [self.tumblrViewController setTumblrWebViewHeight:(self.view.frame.size.height - 44.)];
    
    /* set up navbar */
    self.navbarView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., kNavbarWidth, kNavbarHeight)];
    [self.navbarView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar"]]];
    [self.tumblrViewController.view addSubview:self.navbarView];
    
    self.navbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navbarButton setImage:[UIImage imageNamed:@"navbarButton"] forState:UIControlStateNormal];
    [self.navbarButton setFrame:CGRectMake(0., 0., kNavbarItemSize, kNavbarItemSize)];
    [self.navbarButton addTarget:self action:@selector(didDismissView) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.navbarButton];
    
    transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
}

- (void)openToConference
{
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self presentViewController:self.speakersViewController animated:NO completion:nil];
}

- (void)openToNews
{
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self presentViewController:self.tumblrViewController animated:NO completion:nil];
}

- (void)didDismissView
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
