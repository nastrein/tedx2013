//
//  TXTumblrViewController.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/28/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXTumblrViewController.h"
#import "TXReachability.h"

static const NSString *kTumblrUrl = @"http://news.tedxuofm.com/";

@interface TXTumblrViewController ()
<UIWebViewDelegate>

- (void)webBackAction;
- (void)webForwardAction;

@property (strong, nonatomic) UIWebView *tumblrWebView;
@property (strong, nonatomic) UIView *webNavbar;
@property (assign) BOOL webNavbarHidden;
@property (strong, nonatomic) UIButton *webBack;
@property (strong, nonatomic) UIButton *webForward;

@end

@implementation TXTumblrViewController 

- (id)init
{
    self = [super init];
    if (self) {
        CGFloat screenWidth = self.view.frame.size.width;
        CGFloat screenHeight = self.view.frame.size.height;
        
        self.tumblrWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0., 0., screenWidth, screenHeight)];
        [self.tumblrWebView setScalesPageToFit:YES];
        [self.tumblrWebView setDelegate:self];
        [self.view addSubview:self.tumblrWebView];
        
        /* set up web navbar */
        self.webNavbar = [[UIView alloc] initWithFrame:CGRectMake(0., screenHeight, screenWidth, 60)];
        [self.webNavbar setBackgroundColor:[UIColor colorWithRed:1. green:0.0 blue:0.0 alpha:0.6]];
        [self.tumblrWebView addSubview:self.webNavbar];
        
        self.webBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.webBack setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [self.webBack setFrame:CGRectMake((screenWidth/3) - 9., 21., 18., 18.)];
        [self.webBack addTarget:self action:@selector(webBackAction) forControlEvents:UIControlEventTouchUpInside];
        [self.webBack setEnabled:NO];
        [self.webNavbar addSubview:self.webBack];
        
        self.webForward = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.webForward setBackgroundImage:[UIImage imageNamed:@"forw"] forState:UIControlStateNormal];
        [self.webForward setFrame:CGRectMake((screenWidth/3)*2 - 9., 21., 18., 18.)];
        [self.webForward addTarget:self action:@selector(webForwardAction) forControlEvents:UIControlEventTouchUpInside];
        [self.webForward setEnabled:NO];
        [self.webNavbar addSubview:self.webForward];
        
        [self setWebNavbarHidden:YES];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    if([TXReachability hasInternetConnection]) {
        [self.tumblrWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:(NSString *)kTumblrUrl]]];
    }
}

- (void)setTumblrWebViewHeight:(CGFloat)height
{
    [self.tumblrWebView setFrame:CGRectMake(0., self.view.frame.size.height - height, self.view.frame.size.width, height)];
}

- (void)webBackAction
{
    [self.tumblrWebView goBack];
}

- (void)webForwardAction
{
    [self.tumblrWebView goForward];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(self.tumblrWebView.canGoBack) {
        [self.webBack setEnabled:YES];
    }
    else {
        [self.webBack setEnabled:NO];
    }
    if(self.tumblrWebView.canGoForward) {
        [self.webForward setEnabled:YES];
    }
    else {
        [self.webForward setEnabled:NO];
    }
    
    NSString *currentURL = self.tumblrWebView.request.URL.absoluteString;
    
    if(![currentURL isEqualToString:(NSString *)kTumblrUrl] && [self webNavbarHidden]) {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^(void){
                             [self.webNavbar setFrame:CGRectMake(0., self.view.frame.size.height - 104., self.view.frame.size.width, 104.)];
                         }
                         completion:nil];
        [self setWebNavbarHidden:NO];
    }
    else if([currentURL isEqualToString:(NSString *)kTumblrUrl] && ![self webNavbarHidden]) {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^(void){
                             [self.webNavbar setFrame:CGRectMake(0., self.view.frame.size.height, self.view.frame.size.width, 60.)];
                         }
                         completion:nil];
        [self setWebNavbarHidden:YES];
    }
}

@end
