//
//  TXSpeakerDetailsViewController.h
//  tedx2013
//
//  Created by Nolan Astrein on 2/25/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TXSpeaker;

@interface TXSpeakerDetailsViewController : UIViewController

- (void)configureForSpeaker:(TXSpeaker *)speaker;

@end
