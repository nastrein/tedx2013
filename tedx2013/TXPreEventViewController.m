//
//  TXPreEventViewController.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/19/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXPreEventViewController.h"
#import "TXAppDelegate.h"
#import "TXHomeViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface TXPreEventViewController () {
    CATransition *transition;
}

- (void)updateLogo;
- (void)updateCountDown;
- (void)updateCursor;

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *countDownView;
@property (strong, nonatomic) TXHomeViewController *homeViewController;

@property (strong, nonatomic) UILabel *timerLabel;
@property (strong, nonatomic) UILabel *timeComponentsLabel;
@property (strong, nonatomic) UIImageView *logoView0;
@property (strong, nonatomic) UIImageView *logoView1;
@property (strong, nonatomic) UIImageView *logoView2;
@property (strong, nonatomic) UIImageView *logoView3;
@property (strong, nonatomic) UIView *cursorView;

@property (strong, nonatomic) NSTimer *countDownTimer;
@property (strong, nonatomic) NSTimer *cursorTimer;

@end

static const CGFloat pageNum = 2.;
static const CGFloat sliderWidth = 25.;
static const CGFloat sliderHeight = 75.;
static const CGFloat timerLabelWidth = 260.;
static const CGFloat timerLabelHeight = 60.;
static const CGFloat logoWidth = 268.;
static const CGFloat logoHeight = 34.;

@implementation TXPreEventViewController

- (id)init
{
    self = [super init];
    if(self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    
    /* set up scrollview */
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., 0., screenWidth, screenHeight)];
    self.scrollView.contentSize = CGSizeMake(pageNum*screenWidth, screenHeight);
    [self.scrollView setPagingEnabled:YES];
    [self.view addSubview:self.scrollView];
    
    self.countDownView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., screenWidth, screenHeight)];
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    if (screenRect.size.height == 568) {
        [self.countDownView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"welcome_preevent_568h"]]];
    }
    else {
        [self.countDownView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"welcome_preevent"]]];
    }
    [self.scrollView addSubview:self.countDownView];
    
    UIImageView *sliderLeft = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth - sliderWidth, (screenHeight - sliderHeight)/2, sliderWidth, sliderHeight)];
    [sliderLeft setImage:[UIImage imageNamed:@"sliderbutton_left"]];
    [self.countDownView addSubview:sliderLeft];
    
    /* set up home view controller */
    self.homeViewController = [[TXHomeViewController alloc] init];
    [self.homeViewController.view setFrame:CGRectMake(screenWidth, 0., screenWidth, screenHeight)];
    [self.scrollView addSubview:self.homeViewController.view];
    
    UIImageView *sliderRight = [[UIImageView alloc] initWithFrame:CGRectMake(0., (screenHeight - sliderHeight)/2, sliderWidth, sliderHeight)];
    [sliderRight setImage:[UIImage imageNamed:@"sliderbutton_right"]];
    [self.homeViewController.view addSubview:sliderRight];
    
    /* set up logos */
    CGRect logoFrame = CGRectMake(15., 128., logoWidth, logoHeight);
    self.logoView0 = [[UIImageView alloc] initWithFrame:logoFrame];
    [self.logoView0 setImage:[UIImage imageNamed:@"logo_0"]];
    [self.logoView0 setAlpha:0.0];
    [self.countDownView addSubview:self.logoView0];
    
    self.logoView1 = [[UIImageView alloc] initWithFrame:logoFrame];
    [self.logoView1 setImage:[UIImage imageNamed:@"logo_1"]];
    [self.logoView1 setAlpha:0.0];
    [self.countDownView addSubview:self.logoView1];
    
    self.logoView2 = [[UIImageView alloc] initWithFrame:logoFrame];
    [self.logoView2 setImage:[UIImage imageNamed:@"logo_2"]];
    [self.logoView2 setAlpha:0.0];
    [self.countDownView addSubview:self.logoView2];
    
    self.logoView3 = [[UIImageView alloc] initWithFrame:logoFrame];
    [self.logoView3 setImage:[UIImage imageNamed:@"logo_3"]];
    [self.logoView3 setAlpha:0.0];
    [self.countDownView addSubview:self.logoView3];
    [self updateLogo];
    
    /* set up timer label */
    self.timerLabel = [[UILabel alloc] initWithFrame:CGRectMake((screenWidth - timerLabelWidth)/2, 185., timerLabelWidth, timerLabelHeight)];
    [self.timerLabel setTextAlignment:NSTextAlignmentCenter];
    [self.timerLabel setBackgroundColor:[UIColor clearColor]];
    [self.timerLabel setFont:[UIFont fontWithName:@"GillSans" size:30.]];
    [self.timerLabel setTextColor:[UIColor whiteColor]];
    [self.countDownView addSubview:self.timerLabel];
    [self updateCountDown];
    
    /* set up time components label */
    self.timeComponentsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 225., screenWidth, 30.)];
    [self.timeComponentsLabel setTextAlignment:NSTextAlignmentLeft];
    [self.timeComponentsLabel setBackgroundColor:[UIColor clearColor]];
    [self.timeComponentsLabel setFont:[UIFont fontWithName:@"GillSans" size:15.]];
    [self.timeComponentsLabel setText:@"        months    days     hours   minutes seconds"];
    [self.timeComponentsLabel setTextColor:[UIColor whiteColor]];
    [self.countDownView addSubview:self.timeComponentsLabel];
    
    /* set up cursor */
    self.cursorView = [[UIView alloc] initWithFrame:CGRectMake(283., 161., 22., 2.)];
    [self.cursorView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cursor"]]];
    [self.countDownView addSubview:self.cursorView];
    [self updateCursor];
    
    transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    
    /* kick off timers */
    [self startTimers];
    
}

- (void)startTimers
{
    self.countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                           target:self
                                                         selector:@selector(updateCountDown)
                                                         userInfo:nil
                                                          repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.countDownTimer forMode:NSRunLoopCommonModes];
    
    
    self.cursorTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                        target:self
                                                      selector:@selector(updateCursor)
                                                      userInfo:nil
                                                       repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.cursorTimer forMode:NSRunLoopCommonModes];
}

- (void)updateLogo
{
    [UIView animateWithDuration:2.
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^(void) {
                         [self.logoView0 setAlpha:1.0];
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:2.
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^(void) {
                                              [self.logoView1 setAlpha:1.0];
                                          }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:2.
                                                                    delay:0.0
                                                                  options:UIViewAnimationOptionCurveLinear
                                                               animations:^(void) {
                                                                   [self.logoView2 setAlpha:1.0];
                                                               }
                                                               completion:^(BOOL finished) {
                                                                   [UIView animateWithDuration:2.
                                                                                         delay:0.0
                                                                                       options:UIViewAnimationOptionCurveLinear
                                                                                    animations:^(void) {
                                                                                        [self.logoView3 setAlpha:1.0];
                                                                                    }
                                                                                    completion:^(BOOL finished) {
                                                                                       [UIView animateWithDuration:2.
                                                                                                             delay:0.0
                                                                                                           options:UIViewAnimationOptionCurveLinear
                                                                                                        animations:^(void) {
                                                                                                            [self.logoView3 setAlpha:0.0];
                                                                                                        }
                                                                                                        completion:^(BOOL finished) {
                                                                                                            [UIView animateWithDuration:2.
                                                                                                                                  delay:0.0
                                                                                                                                options:UIViewAnimationOptionCurveLinear
                                                                                                                             animations:^(void) {
                                                                                                                                 [self.logoView2 setAlpha:0.0];
                                                                                                                             }
                                                                                                                             completion:^(BOOL finished) {
                                                                                                                                 [UIView animateWithDuration:2.
                                                                                                                                                       delay:0.0
                                                                                                                                                     options:UIViewAnimationOptionCurveLinear
                                                                                                                                                  animations:^(void) {
                                                                                                                                                      [self.logoView1 setAlpha:0.0];
                                                                                                                                                  }
                                                                                                                                                  completion:^(BOOL finished) {
                                                                                                                                                      [UIView animateWithDuration:2.
                                                                                                                                                                            delay:0.0
                                                                                                                                                                          options:UIViewAnimationOptionCurveLinear
                                                                                                                                                                       animations:^(void) {
                                                                                                                                                                           [self.logoView0 setAlpha:0.0];
                                                                                                                                                                       }
                                                                                                                                                                       completion:^(BOOL finished) {
                                                                                                                                                                           [self updateLogo];
                                                                                                                                                                       }];
                                                                                                                                                  }];
                                                                                                                             }];
                                                                                                        }];
                                                                                    }];
                                                               }];
                                          }];
                     }];
}

- (void)updateCountDown
{
    if([(NSDate *)[NSDate date] compare:[(TXAppDelegate *)[[UIApplication sharedApplication] delegate] conferenceDate]] == NSOrderedDescending) {
        TXHomeViewController *newHomeViewController = [[TXHomeViewController alloc] init];
        [self presentViewController:newHomeViewController animated:YES completion:nil];
        [self.countDownTimer invalidate];
        return;
    }
    
    NSCalendar *calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendarUnit unitFlags = NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *conversionInfo = [calendar components:unitFlags
                                                   fromDate:[NSDate date]
                                                     toDate:[(TXAppDelegate *)[[UIApplication sharedApplication] delegate] conferenceDate]
                                                    options:0];
    
    int months = [conversionInfo month];
    NSString *monthsString = [NSString stringWithFormat:@"%d", months];
    if(months < 10) {
        monthsString = [NSString stringWithFormat:@"0%@", monthsString];
    }
    
    int days = [conversionInfo day];
    NSString *daysString = [NSString stringWithFormat:@"%d", days];
    if(days < 10) {
        daysString = [NSString stringWithFormat:@"0%@", daysString];
    }
    
    int hours = [conversionInfo hour];
    NSString *hoursString = [NSString stringWithFormat:@"%d", hours];
    if(hours < 10) {
        hoursString = [NSString stringWithFormat:@"0%@", hoursString];
    }
    
    int minutes = [conversionInfo minute];
    NSString *minutesString = [NSString stringWithFormat:@"%d", minutes];
    if(minutes < 10) {
        minutesString = [NSString stringWithFormat:@"0%@", minutesString];
    }
    
    int seconds = [conversionInfo second];
    NSString *secondsString = [NSString stringWithFormat:@"%d", seconds];
    if(seconds < 10) {
        secondsString = [NSString stringWithFormat:@"0%@", secondsString];
    }
    
    [self.timerLabel setText:[NSString stringWithFormat:@"%@ : %@ : %@ : %@ : %@", monthsString, daysString, hoursString, minutesString, secondsString]];
}

- (void)updateCursor
{
    if(self.cursorView.hidden) {
        [self.cursorView setHidden:NO];
    }
    else {
        [self.cursorView setHidden:YES];
    }
}

- (void)didDismissView
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
