//
//  TXSpeaker.h
//  tedx2013
//
//  Created by Nolan Astrein on 2/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXSerializable.h"

@interface TXSpeaker : NSObject <TXSerializable>

@property (assign, nonatomic) int speakerID;
@property (assign, nonatomic) int year;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *subject;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *youtubeID;
@property (strong, nonatomic) UIImage *thumbnail;

@end
