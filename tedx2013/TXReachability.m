//
//  TXReachability.m
//  tedx2013
//
//  Created by Nolan Astrein on 3/12/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXReachability.h"
#import "Reachability.h"

@implementation TXReachability

+ (BOOL)hasInternetConnection
{
    Reachability *r = [Reachability reachabilityForInternetConnection];
    if (![r isReachable]) {
        [[[UIAlertView alloc]initWithTitle:@"Aww Snaps!"
                                   message:@"You are not connected to the Internet."
                                  delegate:self
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}

@end
