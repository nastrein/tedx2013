//
//  main.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/19/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TXAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TXAppDelegate class]));
    }
}
