//
//  TXPerson.m
//  tedx2013
//
//  Created by Nolan Astrein on 3/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXPerson.h"

@implementation TXPerson

- (id)initFromDataObject:(NSDictionary *)object
{
    if (self = [super init]) {
        [self setPersonID:[[object objectForKey:@"id"] intValue]];
        [self setAffiliation:[object objectForKey:@"affiliation"]];
        [self setDepartment:[object objectForKey:@"department"]];
        [self setStory1:[object objectForKey:@"short_answer_1"]];
        [self setStory2:[object objectForKey:@"short_answer_2"]];
        [self setStory3:[object objectForKey:@"short_answer_3"]];
        [self setShare:[[object objectForKey:@"share"] boolValue]];
    }
    return self;
}

@end
