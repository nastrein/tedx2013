//
//  TXSpeakersViewController.h
//  tedx2013
//
//  Created by Nolan Astrein on 2/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TXSpeakersViewControllerDelegate <NSObject>

@required
- (void)didDismissView;

@end

@interface TXSpeakersViewController : UIViewController

@property (weak, nonatomic) id<TXSpeakersViewControllerDelegate> delegate;

@end
