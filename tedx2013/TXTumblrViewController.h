//
//  TXTumblrViewController.h
//  tedx2013
//
//  Created by Nolan Astrein on 2/28/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TXTumblrViewController : UIViewController

- (void)setTumblrWebViewHeight:(CGFloat)height;

@end
