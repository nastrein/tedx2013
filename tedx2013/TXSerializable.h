//
//  Serializable.h
//  tedx2013
//
//  Created by Nolan Astrein on 3/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

@protocol TXSerializable <NSObject>

@required
- (id)initFromDataObject:(NSDictionary*) object;

@end
