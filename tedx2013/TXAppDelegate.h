//
//  TXAppDelegate.h
//  tedx2013
//
//  Created by Nolan Astrein on 2/19/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TXPreEventViewController;
@class TXHomeViewController;

@interface TXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TXHomeViewController *homeViewController;
@property (strong, nonatomic) TXPreEventViewController *preEventViewController;

@property (strong, nonatomic) NSDate *conferenceDate;

@end
