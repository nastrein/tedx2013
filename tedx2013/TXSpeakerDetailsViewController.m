//
//  TXSpeakerDetailsViewController.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/25/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXSpeakerDetailsViewController.h"
#import "TXSpeaker.h"
#import "UITextView+DisableCopyPaste.h"
#import <QuartzCore/QuartzCore.h>

@interface TXSpeakerDetailsViewController ()
<UITextViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIImageView *speakerImageView;
@property (strong, nonatomic) UILabel *speakerLabel;
@property (strong, nonatomic) UILabel *subjectLabel;
@property (strong, nonatomic) UITextView *descriptionView;
@property (strong, nonatomic) NSString *youtubeID;

@property (strong, nonatomic) UIView *whiteLayer;
@property (strong, nonatomic) UIView *bottomBar;
@property (strong, nonatomic) UIButton *dismissButton;
@property (strong, nonatomic) UIButton *playButton;
@property (strong, nonatomic) UIWebView *webView;

@end

@implementation TXSpeakerDetailsViewController

- (id)init
{
    self = [super init];
    if (self) {
        CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
        
        NSMutableArray *viewObjects = [[NSMutableArray alloc] init];

        [self.view setBackgroundColor:[UIColor colorWithRed:239./255. green:239./255. blue:239./255. alpha:1.0]];
        
        self.whiteLayer = [[UIView alloc] initWithFrame:CGRectMake(10., 10., screenRect.size.width - 20., screenRect.size.height - 20.)];
        [self.whiteLayer setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:self.whiteLayer];
        [viewObjects addObject:self.whiteLayer];
        
        self.speakerImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 200.)/2, 15., 205., 205.)];
        [self.view addSubview:self.speakerImageView];
        [viewObjects addObject:self.speakerImageView];
        
        self.speakerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20., 210., 280., 30.)];
        [self.speakerLabel setTextAlignment:NSTextAlignmentLeft];
        [self.speakerLabel setNumberOfLines:1];
        [self.speakerLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:20.]];
        [self.speakerLabel setAdjustsFontSizeToFitWidth:YES];
        [self.speakerLabel setAdjustsLetterSpacingToFitWidth:YES];
        [self.speakerLabel setTextColor:[UIColor blackColor]];
        [self.view addSubview:self.speakerLabel];
        [viewObjects addObject:self.speakerLabel];
        
        self.subjectLabel = [[UILabel alloc] initWithFrame:CGRectMake(20., 240., 280., 20.)];
        [self.subjectLabel setTextAlignment:NSTextAlignmentLeft];
        [self.subjectLabel setFont:[UIFont fontWithName:@"Georgia-Italic" size:16.]];
        [self.subjectLabel setAdjustsFontSizeToFitWidth:YES];
        [self.subjectLabel setAdjustsLetterSpacingToFitWidth:YES];
        [self.subjectLabel setTextColor:[UIColor blackColor]];
        [self.view addSubview:self.subjectLabel];
        [viewObjects addObject:self.subjectLabel];
        
        CGRect descriptionFrame;
        if (screenRect.size.height == 568) {
            descriptionFrame = CGRectMake(20., 260., 280., 295.);
        }
        else {
            descriptionFrame = CGRectMake(20., 260., 280., 205.);
        }
        
        self.descriptionView = [[UITextView alloc] initWithFrame:descriptionFrame];
        [self.descriptionView setTextAlignment:NSTextAlignmentLeft];
        [self.descriptionView setUserInteractionEnabled:YES];
        [self.descriptionView setEditable:NO];
        [self.descriptionView setDelegate:self];
        [self.descriptionView setFont:[UIFont fontWithName:@"Helvetica-Light" size:18.]];
        [self.descriptionView setTextColor:[UIColor blackColor]];
        [self.view addSubview:self.descriptionView];
        [viewObjects addObject:self.descriptionView];
        
        self.bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0., screenRect.size.height - 40., screenRect.size.width, 40.)];
        [self.bottomBar setBackgroundColor:[UIColor colorWithRed:0. green:0. blue:0. alpha:.4]];
        [self.view addSubview:self.bottomBar];
        
        self.dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.dismissButton setFrame:CGRectMake(7., 0., 40., 40.)];
        [self.dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [self.dismissButton setBackgroundColor:[UIColor clearColor]];
        [self.dismissButton setTitle:@"\uf05c" forState:UIControlStateNormal];
        [self.dismissButton.titleLabel setFont:[UIFont fontWithName:@"fontawesome" size:35.]];
        [self.dismissButton.titleLabel setTextColor:[UIColor whiteColor]];
        [self.bottomBar addSubview:self.dismissButton];
        
        self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.playButton setFrame:CGRectMake(screenRect.size.width - 47., 0., 40., 40.)];
        [self.playButton addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
        [self.playButton setBackgroundColor:[UIColor clearColor]];
        [self.playButton setTitle:@"\uf01d" forState:UIControlStateNormal];
        [self.playButton.titleLabel setFont:[UIFont fontWithName:@"fontawesome" size:35.]];
        [self.playButton.titleLabel setTextColor:[UIColor whiteColor]];
        [self.bottomBar addSubview:self.playButton];
        
        for (UIView *view in viewObjects) {
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showHideBottomBar:)];
            [tapGesture setNumberOfTapsRequired:1];
            [view addGestureRecognizer:tapGesture];
        }
    }
    return self;
}

- (void)configureForSpeaker:(TXSpeaker *)speaker
{
    [self.speakerImageView setImage:[speaker thumbnail]];
    [self.speakerLabel setText:[[speaker name] uppercaseString]];
    [self.subjectLabel setText:[speaker subject]];
    [self.descriptionView setText:[speaker description]];
    [self.descriptionView setContentOffset:CGPointZero animated:NO];
    [self setYoutubeID:[speaker youtubeID]];
}

- (void)showHideBottomBar:(id)sender {
    if([self.bottomBar isHidden]) {
        [self.bottomBar setHidden:NO];
    }
    else {
        [self.bottomBar setHidden:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.descriptionView flashScrollIndicators];
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:^(void){
        [self.webView removeFromSuperview];
        [self.playButton setTitle:@"\uf01d" forState:UIControlStateNormal];
        [self.playButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [self.playButton addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)playVideo
{
    NSString *urlString = [NSString stringWithFormat:@"http://m.youtube.com/watch?gl=US&hl=en&client=mv-google&v="];
    urlString = [urlString stringByAppendingString:self.youtubeID];
    NSURL *videoUrl = [NSURL URLWithString:urlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:videoUrl];
    
    CGRect oldframe = self.view.frame;
    CGRect newFrame = CGRectMake(0., oldframe.origin.y, oldframe.size.width, oldframe.size.height);
    self.webView = [[UIWebView alloc] initWithFrame:newFrame];
    [self.webView loadRequest:requestObj];
    [self.view insertSubview:self.webView belowSubview:self.bottomBar];
    
    [self.playButton setTitle:@"\uf05e" forState:UIControlStateNormal];
    [self.playButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.playButton addTarget:self action:@selector(stopVideo) forControlEvents:UIControlEventTouchUpInside];
}

- (void)stopVideo
{
    [self.webView removeFromSuperview];
    [self.playButton setTitle:@"\uf01d" forState:UIControlStateNormal];
    [self.playButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.playButton addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    [textView resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
