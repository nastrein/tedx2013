//
//  TXSpeaker.m
//  tedx2013
//
//  Created by Nolan Astrein on 2/24/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "TXSpeaker.h"

@implementation TXSpeaker

#pragma mark - TXSerializable

- (id)initFromDataObject:(NSDictionary *)object
{
    if (self = [super init]) {
        [self setSpeakerID:[[object objectForKey:@"id"] intValue]];
        [self setYear:[[object objectForKey:@"year"] intValue]];
        [self setName:[object objectForKey:@"speaker"]];
        [self setSubject:[object objectForKey:@"subject"]];
        [self setDescription:[object objectForKey:@"description"]];
        [self setYoutubeID:[object objectForKey:@"youtube_id"]];

        if(self.year == 2010) {
            NSURL *url = [NSURL URLWithString:[object objectForKey:@"thumbnail"]];
            NSData *data = [[NSData alloc] initWithContentsOfURL:url];
            self.thumbnail = [[UIImage alloc] init];
            if(data == nil) {
                [self setThumbnail:[UIImage imageNamed:@"placeholder"]];
            }
            else {
                [self setThumbnail:[UIImage imageWithData:data]];
            }
        }
    }
    return self;
}

@end
