//
//  UITextView+DisableCopyPaste.h
//  tedx2013
//
//  Created by Nolan Astrein on 2/26/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (DisableCopyPaste)

- (BOOL)canBecomeFirstResponder;

@end
